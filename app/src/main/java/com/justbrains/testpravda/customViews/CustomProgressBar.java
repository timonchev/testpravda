package com.justbrains.testpravda.customViews;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.justbrains.testpravda.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class CustomProgressBar extends LinearLayout implements Runnable {


    private Context mContext;
    private ArrayList<ImageView> mImageViewHolders;
    private ArrayList<String> mImages;
    private Thread mThreadOfAnimation;
    private boolean is_stop = true;

    public CustomProgressBar(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        mContext = context;
        prepareLayout();
    }

    public CustomProgressBar(Context context){
        super(context);
        mContext = context;
        prepareLayout();
    }

    public void dismiss(){
        is_stop = true;
        setVisibility(View.GONE);
        mThreadOfAnimation.destroy();
    }

    private void prepareLayout() {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_progress_bar, null);
        addView(view);

        mImageViewHolders = new ArrayList<ImageView>();
        mImageViewHolders.add((ImageView) view.findViewById(R.id.imgOne));
        mImageViewHolders.add((ImageView) view.findViewById(R.id.imgTwo));
        mImageViewHolders.add((ImageView) view.findViewById(R.id.imgThree));

        // Prepare an array list of images to be animated
        mImages = new ArrayList<String>();

        mImages.add("shape_white");
        mImages.add("shape3");
        mImages.add("shape2");

        mImages.add("shape1");
        //mImages.add("progress_5");
        //mImages.add("progress_6");

    }

    /**
     * Starting of the animation thread
     */
    public void startAnimation() {
        setVisibility(View.VISIBLE);
        mThreadOfAnimation = new Thread(this, "Progress");
        mThreadOfAnimation.start();
    }


    @Override
    public void run() {
        while (is_stop) {
            try {
                // Will sleep for 0.3 secs and after that change the images
                Thread.sleep(300);
                handler.sendEmptyMessage(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            int currentImage = 0;
            // changing images

                if ( mImageViewHolders.get(0).getVisibility() == View.GONE && mImageViewHolders.get(1).getVisibility() == View.GONE && mImageViewHolders.get(2).getVisibility() == View.GONE){
                    mImageViewHolders.get(0).setVisibility(View.VISIBLE);
                    mImageViewHolders.get(1).setVisibility(View.GONE);
                    mImageViewHolders.get(2).setVisibility(View.GONE);
                    currentImage = 0;
                } else if (mImageViewHolders.get(0).getVisibility() == View.VISIBLE && mImageViewHolders.get(1).getVisibility() == View.GONE && mImageViewHolders.get(2).getVisibility() == View.GONE){
                    mImageViewHolders.get(0).setVisibility(View.VISIBLE);
                    mImageViewHolders.get(1).setVisibility(View.VISIBLE);
                    mImageViewHolders.get(2).setVisibility(View.GONE);
                    currentImage = currentImage + 1;
                }else  if (mImageViewHolders.get(0).getVisibility() == View.VISIBLE && mImageViewHolders.get(1).getVisibility() == View.VISIBLE && mImageViewHolders.get(2).getVisibility() == View.GONE) {
                    mImageViewHolders.get(0).setVisibility(View.VISIBLE);
                    mImageViewHolders.get(1).setVisibility(View.VISIBLE);
                    mImageViewHolders.get(2).setVisibility(View.VISIBLE);
                    currentImage = currentImage + 1;
                }

            else if (mImageViewHolders.get(0).getVisibility() == View.VISIBLE && mImageViewHolders.get(1).getVisibility() == View.VISIBLE && mImageViewHolders.get(2).getVisibility() == View.VISIBLE) {
                mImageViewHolders.get(0).setVisibility(View.GONE);
                mImageViewHolders.get(1).setVisibility(View.GONE);
                mImageViewHolders.get(2).setVisibility(View.GONE);
            }
                //imageView.setTag("" + nextImage);
                //imageView.setImageResource(getResources().getIdentifier(
                   //     mImages.get(nextImage - 1), "drawable",
                  //      mContext.getPackageName()));

            super.handleMessage(msg);
        }

    };


}
