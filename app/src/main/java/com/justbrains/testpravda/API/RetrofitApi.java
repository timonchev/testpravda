package com.justbrains.testpravda.API;

import com.justbrains.testpravda.models.ResponseCheckAuth;
import com.justbrains.testpravda.models.ResponseModelUser;
import com.justbrains.testpravda.models.ResponseOfOrders;
import com.justbrains.testpravda.models.UserAuthResponse;


import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public interface RetrofitApi {

    @POST("/?user.auth")
    public void userAuth(@Body TypedInput input, Callback<ResponseCheckAuth> callback);

    @POST("/?user.verify")
    public void userVerify(@Body TypedInput input, Callback<ResponseModelUser> callback );

    @POST("/?order.getList")
    public void getList(@Body TypedInput input, Callback<ResponseOfOrders> callback);

    @POST("/?user.logout")
    public void logOut(@Body TypedInput input, Callback<ResponseCheckAuth> callback);
}
