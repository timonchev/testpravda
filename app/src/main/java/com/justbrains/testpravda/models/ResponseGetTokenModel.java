package com.justbrains.testpravda.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandertimonchev on 14.08.15.
 */


@Parcel
public class ResponseGetTokenModel {
    @SerializedName("id")
    private String id;
    @SerializedName("auth_token")
    private String authToken;
    @SerializedName("err")
    private List<Integer> err = new ArrayList<Integer>();
    @SerializedName("err_msg")
    private List<String> errMsg = new ArrayList<String>();

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The authToken
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * @param authToken The auth_token
     */
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     * @return The err
     */
    public List<Integer> getErr() {
        return err;
    }

    /**
     * @param err The err
     */
    public void setErr(List<Integer> err) {
        this.err = err;
    }

    /**
     * @return The errMsg
     */
    public List<String> getErrMsg() {
        return errMsg;
    }

    /**
     * @param errMsg The err_msg
     */
    public void setErrMsg(List<String> errMsg) {
        this.errMsg = errMsg;
    }

}
