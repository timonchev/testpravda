package com.justbrains.testpravda.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class UserAuthResponse {
    @SerializedName("err")
    private List<Integer> err = new ArrayList<Integer>();
    @SerializedName("err_msg")
    private List<String> errMsg = new ArrayList<String>();

    /**
     *
     * @return
     * The err
     */
    public List<Integer> getErr() {
        return err;
    }

    /**
     *
     * @param err
     * The err
     */
    public void setErr(List<Integer> err) {
        this.err = err;
    }

    /**
     *
     * @return
     * The errMsg
     */
    public List<String> getErrMsg() {
        return errMsg;
    }

    /**
     *
     * @param errMsg
     * The err_msg
     */
    public void setErrMsg(List<String> errMsg) {
        this.errMsg = errMsg;
    }

}
