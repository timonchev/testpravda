package com.justbrains.testpravda.models;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class ResponseCheckAuth {
    public UserAuthResponse getResponse() {
        return response;
    }

    public void setResponse(UserAuthResponse response) {
        this.response = response;
    }

    private UserAuthResponse response;
}
