package com.justbrains.testpravda.models;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class Order {

    private String title;
    private String budget;
    private String created_at;
    private String author;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getNew_messages() {
        return new_messages;
    }

    public void setNew_messages(String new_messages) {
        this.new_messages = new_messages;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    private String avatar;
    private String new_messages;
    private String tags;

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The budget
     */
    public String getBudget() {
        return budget;
    }

    /**
     *
     * @param budget
     * The budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return created_at;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }


}