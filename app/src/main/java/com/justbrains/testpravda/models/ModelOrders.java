package com.justbrains.testpravda.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class ModelOrders {

    private List<Order> orders = new ArrayList<Order>();
    private List<Integer> err = new ArrayList<Integer>();
    private List<String> errMsg = new ArrayList<String>();

    /**
     *
     * @return
     * The orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /**
     *
     * @return
     * The err
     */
    public List<Integer> getErr() {
        return err;
    }

    /**
     *
     * @param err
     * The err
     */
    public void setErr(List<Integer> err) {
        this.err = err;
    }

    /**
     *
     * @return
     * The errMsg
     */
    public List<String> getErrMsg() {
        return errMsg;
    }

    /**
     *
     * @param errMsg
     * The err_msg
     */
    public void setErrMsg(List<String> errMsg) {
        this.errMsg = errMsg;
    }


}
