package com.justbrains.testpravda.models;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class ResponseOfOrders {
    public ModelOrders getResponse() {
        return response;
    }

    public void setResponse(ModelOrders response) {
        this.response = response;
    }

    private ModelOrders response;
}
