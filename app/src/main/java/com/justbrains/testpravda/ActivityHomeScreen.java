package com.justbrains.testpravda;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.justbrains.testpravda.Fragments.HomeFragment;
import com.justbrains.testpravda.Fragments.ProfileFragment;
import com.justbrains.testpravda.Fragments.RelatedFragment;
import com.justbrains.testpravda.utils.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class ActivityHomeScreen extends FragmentActivity implements View.OnClickListener {
    @Bind(R.id.title_text)
    TextView titleText;
    @Bind(R.id.layout_top)
    LinearLayout layoutTop;
    @Bind(R.id.main_fragment)
    FrameLayout mainFragment;
    @Bind(R.id.ll_home)
    LinearLayout llHome;
    @Bind(R.id.ll_forms)
    LinearLayout llForms;
    @Bind(R.id.ll_profile)
    LinearLayout llProfile;
    @Bind(R.id.icon_home)
    ImageView iconHome;
    @Bind(R.id.view_home)
    View viewHome;
    @Bind(R.id.icon_related)
    ImageView iconRelated;
    @Bind(R.id.view_related)
    View viewRelated;
    @Bind(R.id.icon_profile)
    ImageView iconProfile;
    @Bind(R.id.view_profile)
    View viewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        changeFragment(HomeFragment.newInstance());
    }

    @Override
    public void onClick(View v) {

    }

    @OnClick(R.id.ll_home)
    public void home (View view)
    {
        iconHome.setImageResource(R.drawable.my_active);
        iconProfile.setImageResource(R.drawable.profile_inactive);
        iconRelated.setImageResource(R.drawable.forme_inactive);
        viewProfile.setVisibility(View.GONE);
        viewRelated.setVisibility(View.GONE);
        viewHome.setVisibility(View.VISIBLE);
        titleText.setText("Мои");
        changeFragment(new HomeFragment().newInstance());
    }

    @OnClick(R.id.ll_forms)
    public void forms(View view)
    {
        iconHome.setImageResource(R.drawable.my_inactive);
        iconProfile.setImageResource(R.drawable.profile_inactive);
        iconRelated.setImageResource(R.drawable.forme_active);
        viewProfile.setVisibility(View.GONE);
        viewRelated.setVisibility(View.VISIBLE);
        viewHome.setVisibility(View.GONE);
        titleText.setText("Подходящие");

        changeFragment(new RelatedFragment().newInstance());
    }

    @OnClick(R.id.ll_profile)
    public void profile(View view)
    {
        iconHome.setImageResource(R.drawable.my_inactive);
        iconProfile.setImageResource(R.drawable.profile_active);
        iconRelated.setImageResource(R.drawable.forme_inactive);
        viewProfile.setVisibility(View.VISIBLE);
        viewRelated.setVisibility(View.GONE);
        viewHome.setVisibility(View.GONE);
        titleText.setText("Профиль");

        changeFragment(new ProfileFragment().newInstance());
    }

    protected void changeFragment(android.support.v4.app.Fragment targetFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                        //.setCustomAnimations(R.anim.popup_enter, R.anim.popup_exit)
                .add(R.id.main_fragment, targetFragment, "tag")
                .addToBackStack(Constants.POST_LIST_FRAGMENT)
                .setTransitionStyle(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping back stack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on back stack, calling super");
            super.onBackPressed();
        }
    }
}
