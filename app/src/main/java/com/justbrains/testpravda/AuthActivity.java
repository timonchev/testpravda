package com.justbrains.testpravda;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.justbrains.testpravda.API.RetrofitApi;
import com.justbrains.testpravda.R;
import com.justbrains.testpravda.models.ResponseCheckAuth;
import com.justbrains.testpravda.models.UserAuthResponse;
import com.justbrains.testpravda.utils.Constants;
import com.justbrains.testpravda.utils.MaskedWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class AuthActivity extends Activity{

    @Bind(R.id.spinner)
    Spinner spinner;
    @Bind(R.id.edit_phone_number)
    EditText editPhoneNumber;

    String[] strings = {"+7","+38","+1"};
    int arr_images[] = {R.drawable.rus, R.drawable.ua, R.drawable.usa};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        spinner.setAdapter(new MyAdapter(this, R.layout.spinner_item, strings));

        editPhoneNumber.addTextChangedListener(new MaskedWatcher("### ### ## ##"));
    }

    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_item, parent, false);
            TextView label=(TextView)row.findViewById(R.id.textView_code_country);
            label.setText(strings[position]);


            ImageView icon=(ImageView)row.findViewById(R.id.imageView_image_country);
            icon.setImageResource(arr_images[position]);

            return row;
        }
    }


    @OnClick(R.id.btn_enter_login)
    public void enterToSystem(View view) {
        JSONObject jsonobj = new JSONObject();
        //spinner.getSelectedItemPosition();
        final String code = strings[spinner.getSelectedItemPosition()].replace("+", "");
        String phone =  editPhoneNumber.getText().toString().replaceAll(" ", "");
        try {
            jsonobj.put("phone_in", phone);
            jsonobj.put("phone_code", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = jsonobj.toString();

        TypedInput in = null;
        try {
            in = new TypedByteArray("application/json", json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {

            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(requestInterceptor).setEndpoint(Constants.API_URI).build();
        RetrofitApi myApi = restAdapter.create(RetrofitApi.class);

        myApi.userAuth(in, new Callback<ResponseCheckAuth>() {
            @Override
            public void success(ResponseCheckAuth userAuthResponse, Response response) {
                Log.d("TAG", "succes");
                    //Toast.makeText(AuthActivity.this, "Error, " + userAuthResponse.getErr().get(0).toString(), Toast.LENGTH_SHORT).show();

                if (userAuthResponse.getResponse().getErr().get(0) == 0)
                    startActivity(new Intent(AuthActivity.this, AuthConfirmActivity.class).putExtra("code", code).putExtra("phone", editPhoneNumber.getText().toString()));

            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
            }
        });

    }
}
