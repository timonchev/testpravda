package com.justbrains.testpravda.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.devspark.progressfragment.ProgressFragment;
import com.justbrains.testpravda.API.RetrofitApi;
import com.justbrains.testpravda.R;
import com.justbrains.testpravda.adapters.CustomAdapterHomeList;
import com.justbrains.testpravda.adapters.CustomAdapterRelatedList;
import com.justbrains.testpravda.models.Order;
import com.justbrains.testpravda.models.ResponseOfOrders;
import com.justbrains.testpravda.utils.Constants;
import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class RelatedFragment extends ProgressFragment {

    @Bind(R.id.progress_container)
    LinearLayout progressContainer;
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.content_container)
    RelativeLayout contentContainer;
    private CustomAdapterRelatedList adapter;
    private ArrayList<Order> list;

    public static RelatedFragment newInstance() {
        RelatedFragment fragment = new RelatedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentShown(false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Setup content view
        //setContentShown(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(getActivity());
        list = new ArrayList<Order>();

        listView = (ListView) v.findViewById(R.id.listView);

        adapter = new CustomAdapterRelatedList(getActivity(), list);
        listView.setAdapter(adapter);

        getListRelated();
        return v;
    }

    public void getListRelated() {
        JSONObject jsonobj = new JSONObject();
        //spinner.getSelectedItemPosition();
        String token = Hawk.get("auth_token");
        try {
            jsonobj.put("auth_token", token);
            jsonobj.put("type", "forme");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = jsonobj.toString();

        TypedInput in = null;
        try {
            in = new TypedByteArray("application/json", json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {

            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(requestInterceptor).setEndpoint(Constants.API_URI).build();
        RetrofitApi myApi = restAdapter.create(RetrofitApi.class);

        myApi.getList(in, new Callback<ResponseOfOrders>() {


            @Override
            public void success(ResponseOfOrders userResponse, Response response) {
                Log.d("TAG", "info: " + userResponse.getResponse().getOrders().get(0).getTitle());


                list.addAll(userResponse.getResponse().getOrders());

                //adapter = new CustomAdapterHomeList(getActivity(), list);
                adapter.notifyDataSetChanged();
                setContentShown(true);
            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
