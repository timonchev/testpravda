package com.justbrains.testpravda.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.devspark.progressfragment.ProgressFragment;
import com.justbrains.testpravda.API.RetrofitApi;
import com.justbrains.testpravda.LoginActivity;
import com.justbrains.testpravda.R;
import com.justbrains.testpravda.models.ResponseCheckAuth;
import com.justbrains.testpravda.utils.Constants;
import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class ProfileFragment extends ProgressFragment {


    @Bind(R.id.btn_exit)
    Button btnExit;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentShown(false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Setup content view
        setContentShown(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(getActivity());

        btnExit = (Button) v.findViewById(R.id.btn_exit);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonobj = new JSONObject();
                //spinner.getSelectedItemPosition();

                String token = Hawk.get("auth_token");
                try {
                    jsonobj.put("auth_token", token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String json = jsonobj.toString();

                TypedInput in = null;
                try {
                    in = new TypedByteArray("application/json", json.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                RequestInterceptor requestInterceptor = new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {

                    }
                };

                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .setRequestInterceptor(requestInterceptor).setEndpoint(Constants.API_URI).build();
                RetrofitApi myApi = restAdapter.create(RetrofitApi.class);

                myApi.logOut(in, new Callback<ResponseCheckAuth>() {
                    @Override
                    public void success(ResponseCheckAuth userAuthResponse, Response response) {
                        Log.d("TAG", "succes");
                        //Toast.makeText(AuthActivity.this, "Error, " + userAuthResponse.getErr().get(0).toString(), Toast.LENGTH_SHORT).show();

                        if (userAuthResponse.getResponse().getErr().get(0) == 0) {

                            Hawk.put("auth_token", "null");
                            Intent newIntent = new Intent(getActivity(), LoginActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                            getActivity().finish();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.getLocalizedMessage();
                    }
                });
            }
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
