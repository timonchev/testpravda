package com.justbrains.testpravda.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.justbrains.testpravda.R;
import com.justbrains.testpravda.models.Order;

import java.util.List;

/**
 * Created by alexandertimonchev on 14.08.15.
 */
public class CustomAdapterRelatedList extends BaseAdapter {

    private Context mContext;
    private List<Order> data;
    private static LayoutInflater inflater = null;


    public CustomAdapterRelatedList(Context context, List<Order> list){
        mContext = context;
        data = list;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (convertView == null) {

            v = inflater.inflate(R.layout.item_forms_dialogs, null);

            holder = new ViewHolder();

            holder.text = (TextView) v.findViewById(R.id.text_message);
            holder.budget = (TextView) v.findViewById(R.id.anything_count);
            holder.time = (TextView) v.findViewById(R.id.time_message);
            holder.tags = (TextView) v.findViewById(R.id.tags);

            v.setTag(holder);
        } else{
            holder = (ViewHolder) v.getTag();
        }
        holder.text.setText(data.get(position).getTitle());
        holder.budget.setText(data.get(position).getBudget() + "$");
        holder.time.setText(data.get(position).getCreatedAt());
        holder.tags.setText(data.get(position).getTags());

        return v;
    }

    public static class ViewHolder {
        private TextView text;
        private TextView budget;
        private TextView time;
        private TextView tags;
    }
}

